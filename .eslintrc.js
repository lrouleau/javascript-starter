module.exports = {
    "env": {
        "es6": true,
        "node": true,
        "jest/globals": true
    },
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "indent": [
            "error",
            4
        ],
        /*
        "linebreak-style": [
            "error",
            "windows"
        ],
        */
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "never"
        ]
    },
    "plugins": ["jest"]
};
