// define our function with the callback argument
function some_function(arg1, arg2, callback) {
    var my_number = arg1 + arg2
    // then we're done, so we'll call the callback and pass our result
    callback(my_number)
}
// call the function
some_function(5, 15, function (num) {
    // this anonymous function will run when the callback is called
    console.log('callback called! ' + num)
})

//http://www.recipepuppy.com/api/?i=onions,garlic&q=omelet&p=3 