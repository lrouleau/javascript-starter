export default () => {
    return 'mydefaultmethod'
}

export const mysecondmethod = (i) => {
    return 'mysecondmethod' + i
}

export const drinkFlavor = (flavor) => {
    if (flavor == 'octopus') {
        throw new Error('yuck, octopus flavor')
    }
    // Do some other stuff
}

const change = (num, val, i) => {
    return 'myprivatemethod' + num + val + i
}
