import mydefault, {mysecondmethod, drinkFlavor} from './es6lib'

test('my default method should return mydefaultmethod', () => {
    expect(mydefault()).toBe('mydefaultmethod')
})
test('my default method should return mydefaultmethod', () => {
    expect(mysecondmethod(2)).toBe('mysecondmethod2')
})
test('throws on octopus', () => {
    function drinkOctopus() {
        drinkFlavor('octopus')
    }
  
    // Test the exact error message
    expect(drinkOctopus).toThrowError('yuck, octopus flavor')
  
    // Test that the error message says "yuck" somewhere
    expect(drinkOctopus).toThrowError(/yuck/)
  
    // Test that we get a DisgustingFlavorError
    expect(drinkOctopus).toThrowError(Error)
})
