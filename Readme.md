Permet d'initier un environnement préconfigurer pour faire des devs/tests en javascript (ECMAScript 2015) avec jest
La version 2 utilise webpack 4 et ne transpile plus en ES5 avec Babel

[X] Webpack     
[X] Mocha    
[X] ESlint  
[X] Babel   

Jest	
	https://facebook.github.io/jest/
	
ESLint rules    
    https://eslint.org/docs/rules/

To find out more
    https://www.npmjs.com/package/webpack
    Jest
    Mocha
    Chai
    Enzyme (pour le mocks)